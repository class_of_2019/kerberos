"""
    Project
        Kerberos

    Maintainer
        Allen Oyieke
        
    Date 
        31-  12 - 2018
        
    DISCLAIMER :
         For this Assignment I have decided to implment all communication parties using a message queue broker (RabbitMQ) for IPC 
        
    
    This is the KDC  it servers both as authentication server and ticket granting server 
"""

#!/usr/bin/env ruby
require 'bunny'
require 'activesupport'
 require 'time'


# CONSTANTS
CONFIG ="config.txt"
DBNAME = "clients.txt"
TGS = "TGS"
TGSKEY =""
MSGREGEX =%r{:}



KDCINIT = 'kdc_initiator'
KDCRESPONSE = 'kdc_init_response'
TGSINT = 'tgs_init_response'
TGSRESPONSE = 'tgs_response_response'

def displayQuestion(question)
	# Function to display a question

	puts "	[?] #{question}"

	return gets.chomp
end


def displaySimpleMessage(symbol,message)
	# Function to display a message

	puts "#{symbol} #{message}"
end

def displayComplexMessage(symbol,message,content)
	# Function to display a message

	puts "#{symbol} #{message} :[ #{content} ]"
end


def class KDC
	# Class containing all function required to run kdc

	def searchFile(filename,word)
		# Search a file for a phrase

		@@count=0
		@@line=Array.new
		@@line_content =Array.new
		
		File.foreach("#{filename}").with_index do |line, line_num|
			if search=line.scan( /\b#{word}\b/i )
				#puts search

				if  search.any?
					@@count +=1
					@@line =line_num
					@@line_content =line
				end
			end
			
		end
		
		if @@count == 0
			return false

		elsif @@count ==1
			return true,@@line,@@line_content
		else
			return false,@@count,"Error"
		end
	end

	def addClient

		# Function to add new users 
		@@client_name =displayQuestion("Give a name for the new client:")

		if searchFile(DBNAME,client_name)
			# Generate password
			key = SecureRandom.random_bytes(32)

			# Display message
			displayComplexMessage("*","#{client_name} password:","#{key}")

			# Write data to file
			File.open(DBNAME, 'w') { |file| file.write("#{client_name} : #{key} ") }

		else
			displaySimpleMessage("!", "Sorry that name is already taken")
			addClient
		end
	end

	def splitMessage(message,delimiter=MSGREGEX)
		# Funtion to split message based on pattern
		return message.split(MSGREGEX})
	end

	def sendMessageToBroker(channel,message,queue_name)
		# Function to send message 

		channel.default_exchange.publish( "#{message}", routing_key: "#{queue_name}")
	end

	def randomString(len)
		# Function to generate random string

		o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
		 return string = (0...len).map { o[rand(o.length)] }.join

	end

	def getTime
		# Function to return time

		t =Time.new()
		return t,t.strftime("%FT%T%:z")

	end

	def stringtoTime(time)

		return Time.parse(time)
	end

	def startUp(channel)

		# Function to perform basic initialization 

		bool,var1,var2 =kdc.searchFile(CONFIG,TGS)
		if bool
			content= kdc.splitMessage(var2)
			TGSKEY =content[1]
		else
			TGSKEY = displayQuestion("Enter TGS Encryption Key")
		end

		# Create a queue required for communication
		kdc_init_queue = channel.queue("#{KDCINIT}")
		kdc_response_queue = channel.queue("#{KDCRESPONSE}")
		tgs_init_queue =channel.queue("#{TGSINT}")
		tgs_response_queue =channel.queue("#{TGSRESPONSE}")

		return  kdc_init_queue, kdc_response_queue, tgs_init_queue,tgs_response_queue
	end

	def createConnection
		# Function to create a connection to message queue broker

		# Connect to server
		connection = Bunny.new
		connection.start

		channel = connection.create_channel

		return connection,channel
	end


	def encrypt(message,key)
		# Function to encrypt message

		crypt = ActiveSupport::MessageEncryptor.new("#{key}")
		return encrypted_data = crypt.encrypt_and_sign("#{message}")

	end


	def decrypt(message,key)
		# Function to decrypt message

		crypt = ActiveSupport::MessageEncryptor.new("#{key}")
		return decrypted_data =crypt.decrypt_and_verify("#{message}")

	end

	def randomString(len)
		# Function to generate random string

		o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
		 return string = (0...len).map { o[rand(o.length)] }.join

	end

end

kdc = KDC.new

# Initialize communication channels
connection,channel = kdc.createConnection

# Setup communication channels 
kdc_init_queue, kdc_response_queue, tgs_init_queue,tgs_response_queue= kdc.startUp(channel)

# 

begin
	displaySimpleMessage("Waiting for messages. To exit press CTRL+C","*")

	kdc_init_queue.subscribe(block: true) do |_delivery_info, _properties, body|

		message =kdc.splitMessage(body)

		bool,var1,var2 =kdc.searchFile(DBNAME,message[0])
		
		if bool

			bool,var1,var2 =kdc.searchFile(DBNAME,message[1])
			if bool
				client_info=kdc.splitMessage(var2)

			else
				displaySimpleMessage("!","Client does not exists")
				kdc.sendMessageToBroker(channel,"Sorry credentials are incorrect",message[2])

				# Exit

			end

			send_channel = randomString(60)
			recv_channel = randomString(60)
			lifetime=3600
			time,time_str=kdc.getTime
			# TODO: convert time to string and send
			# Load file with tgs key ,send_channel, recv_channel, lifetime, timestamp, 
			message = kdc.encrypt("#{TGSKEY}:#{send_channel}:#{recv_channel}:#{lifetime}:#{time_str}",client_info[1])
			kdc.sendMessageToBroker(channel,"#{message}",message[2])
			kdc.sendMessageToBroker(channel,"#{message}",TGSRESPONSE)

		else
			kdc.sendMessageToBroker(channel,"Sorry credentials are incorrect",message[2])
			
			# Exit
			exit(0)

		end
	end
end

