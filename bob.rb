"""
    Project
        Kerberos

    Maintainer
        Allen Oyieke
        
    Date 
        31-  12 - 2018
        
    DISCLAIMER :
         For this Assignment I have decided to implment all communication parties using a message queue broker (RabbitMQ) for IPC 
        
    
    Bob is a client that operating in environs ( with another client after receiving seesion keys )
"""

#!/usr/bin/env ruby
require 'bunny'
require 'rdoc'
require 'rbnacl'
require 'active_support/core_ext/numeric/time.rb'



# CONSTANTS
CLIENTNAME = "bob"
PASSWD =""
KDCINIT = 'kdc_initiator'
KDCRESPONSE = ""
TGSRESPONSE = 'tgs_response'


class Client

	# Class Variables

	@in_session=false
	@session_data=nil
	@send_channel = nil
	@recv_channel  = nil


	def createConnection
		# Function to create a connection to message queue broker

		# Connect to server
		connection = Bunny.new
		connection.start

		channel = connection.create_channel

		return connection,channel
	end

	def createQueue ()
		# Function to create queue
	end

	def sendMessageToBroker(channel,message,queue_name)
		# Function to send message 

		channel.default_exchange.publish( "#{message}", routing_key: "#{queue_name}")
	end

	def exitProgram(connection)
		# Function to close connection

		connection.close
	end

	def getUserInfo()
		# Function to get user Information (used in validation)
		
		puts " *********************************************************** "
		
		@name = displayQuestion ("Enter your name: ")
		@password =displayQuestion ("Enter your password: ")
		@service = displayQuestion ("Enter name of service requesting: ")

		return name,password,service
	end

	def startUp(channel)

		# Function to perform basic initialization 

		KDCRESPONSE=randomString(40)

		# Create a queue required for communication
		kdc_init_queue = channel.queue("#{KDCINIT}")
		kdc_response_queue = channel.queue("#{KDCRESPONSE}")
		tgs_init_queue =channel.queue("#{TGSINT}")
		tgs_response_queue =channel.queue("#{TGSRESPONSE}")

		# Generate specific queue

		return  kdc_init_queue, kdc_response_queue, tgs_init_queue,tgs_response_queue, custom_queue_name
	end

	def encrypt(message,key)
		# Function to encrypt message

		crypt = ActiveSupport::MessageEncryptor.new("#{key}")
		return encrypted_data = crypt.encrypt_and_sign("#{message}")

	end


	def decrypt(message,key)
		# Function to decrypt message

		crypt = ActiveSupport::MessageEncryptor.new("#{key}")
		return decrypted_data =crypt.decrypt_and_verify("#{message}")

	end

	def randomString(len)
		# Function to generate random string

		o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
		return string = (0...len).map { o[rand(o.length)] }.join

	end

	def getTime
		# Function to return time

		t =Time.new()
		return t,t.strftime("%FT%T%:z")

	end

	def stringtoTime(time)

		return Time.parse(time)
	end

	def splitMessage(message,delimiter=MSGREGEX)
		# Funtion to split message based on pattern
		return message.split(MSGREGEX})
	end

	def randomString(len)
		# Function to generate random string

		o = [('a'..'z'), ('A'..'Z')].map(&:to_a).flatten
		 return string = (0...len).map { o[rand(o.length)] }.join

	end

	def intro
		puts "/*******************************************************************\\"
	    puts "\t\t\tP15/36567/2016 - Kerberos"
	    puts "\\*******************************************************************/"
	end

	def menu

		if @in_session

			@@is_exit= false

			loop do

				if @@is_exit

					break 
				end
		  	
			  	puts "|*******************************************************************|"
			  	displayMenuEntry("s","Send data to client")
			  	displayMenuEntry("l","Listen for response")
			  	displayMenuEntry("e", "Close application")

			    choice = displayQuestion('Enter your choice: ')

				#Check if session key still valid

				time =stringtoTime(@session_data[4])

				current, str_current = getTime()

				if (time +@session_data[3]) < current

					case choice
			    		when 's'
							msg_to_send = displayQuestion("Enter text to send")

							# Send data to client encrypted with session key 
							sendMessageToBroker(channel, encrypted_data(msg_to_send,@session_data[0]),"#{@session_data[]}")

							break
						when "l"
							begin
								displaySimpleMessage("*","\nWaiting for messages. To exit press CTRL+C to go back to main menu")


								recv_channel.subscribe(block: true) do |_delivery_info, _properties, body|
									
									displaySimpleMessage("*","Received new data")

									# decrypt data
									msg = decrypt(body, @session_data[0])
									displaySimpleMessage("+","#{msg}")

							rescue Interrupt => _
								
								@@is_exit =true
								break

							end

							
						end
					end
				
				else
					displaySimpleMessage("!", "Error Session Key has expired , get a new key")
				end

			end

		else
	  		loop do

		  	puts "|*******************************************************************|"
		  	displayMenuEntry("c","Communicate with other client")
		  	displayMenuEntry("r","Wait for communication from another client")
		  	displayMenuEntry("e", "Close application")
		    choice = displayQuestion('Enter your choice: ')

		    case choice
		    	when 'c'

		    		displaySimpleMessage("*" ," Loading...")

				    # Initialize communication channels
				     connection,channel = createConnection

					# Setup communication channels 
					kdc_init_queue, kdc_response_queue, tgs_init_queue,tgs_response_queue= startUp(channel)

					# Get information to send to kdc
					client_name, client_passwd,requested_service = getUserInfo

					# Step #1
					# Send message to kdc


					sendMessageToBroker(channel,"#{client_name}:#{requested_service}:#{KDCRESPONSE}","#{KDCINIT}")

					begin
						displaySimpleMessage("*","Waiting for messages. To exit press CTRL+C")


						kdc_response_queue.subscribe(block: true) do |_delivery_info, _properties, body|
							
							displaySimpleMessage("*","Received data from KDC")

							displaySimpleMessage("+","#{body}")

						    # Proceed to step #2
						    # Decrypt message to obtain tgs session key

						    decrypted_response = decrypt("#{body}",client_passwd)

						    message = splitMessage(decrypted_response)
						    
						    if message.any?

						    	@session_data =message
						    	@in_session = true

						    	displaySimpleMessage("+", "Received Session Key : [ #{session_data} ]")					   

						    	# Initialize send channel to new client
								@send_channel+channel.queue("#{@session_data[1]}")
								@recv_channel+channel.queue("#{@session_data[2]}")


								# break to send menu
								break
						    end

						  end
					rescue Interrupt => _
					  connection.close

					  exit(0)
					end
					break
			    
			    when 'e'
			      exitProgram

				else
					displaySimpleMessage("!" ," Error , enter a designated value.")

				end
			end
		end
	end

end


def displayQuestion(question)
	# Function to display a question

	puts "\n\t[?] #{question} :"

	return gets.chomp
end

def displayMenuEntry(code,message)
	# Function to display a message

	puts "\t #{code} : #{message}"
end


def displaySimpleMessage(symbol,message)
	# Function to display a message

	puts "#{symbol} #{message}"
end

#Instatiate instance of class to call program
bob = Client.new

bob.menu
